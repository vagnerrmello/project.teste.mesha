/****** 
Script de Cria��o das tabelas 

OBSERVA��O
1 - Conect a base desejada;
2 - Abra uma nova Query;
3 - Copie e Cole na base que ir� utiliza;
4 - Clique no bot�o Execute ou (F5) do Sql Server para execu��o do Script.

� necess�rio seguir a sequ�ncia como est� neste script, pois existe depend�ncias entre tabelas e Trigger e a Store Procedure.

******/


/****** Object:  Table [dbo].[tbCorretora] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbCorretora](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](150) NULL,
	[percentual] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tbCorretora] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id da Corretora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCorretora', @level2type=N'COLUMN',@level2name=N'id'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da Corretora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCorretora', @level2type=N'COLUMN',@level2name=N'nome'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este percentual ser� utilizado para chegar ao valor do comissionado; multiplicando o percentual da corretora pelo valor da parcela que est� na tabela tbParcela para chegar a este valor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCorretora', @level2type=N'COLUMN',@level2name=N'percentual'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela que armazena o nome das Corretoras e o percentual pertencente a mesma' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbCorretora'
GO


/****** Object:  Table [dbo].[tbConveniado] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbConveniado](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](150) NULL,
	[corretoraId] [bigint] NOT NULL,
	[nascimento] [date] NULL,
 CONSTRAINT [PK_tbConveniado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbConveniado]  WITH CHECK ADD  CONSTRAINT [FK_tbConveniado_tbCorretora] FOREIGN KEY([corretoraId])
REFERENCES [dbo].[tbCorretora] ([id])
GO

ALTER TABLE [dbo].[tbConveniado] CHECK CONSTRAINT [FK_tbConveniado_tbCorretora]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do conveniado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbConveniado', @level2type=N'COLUMN',@level2name=N'nome'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campo para relacionar o conveniado a Corretora atrav�s da tabela tbCorretora' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbConveniado', @level2type=N'COLUMN',@level2name=N'corretoraId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de nascimento do Conveniado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbConveniado', @level2type=N'COLUMN',@level2name=N'nascimento'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela para armazenar o conveniado pertencente a Corretora relacionada atrav�s do campo corretoraId com a tabela tbCorretora.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbConveniado'
GO


/****** Object:  Table [dbo].[tbParcela] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbParcela](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[valor] [decimal](18, 2) NULL,
	[vencimento] [date] NULL,
	[conveniadoId] [bigint] NOT NULL,
 CONSTRAINT [PK_tbParcela] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbParcela]  WITH CHECK ADD  CONSTRAINT [FK_tbParcela_tbConveniado] FOREIGN KEY([conveniadoId])
REFERENCES [dbo].[tbConveniado] ([id])
GO

ALTER TABLE [dbo].[tbParcela] CHECK CONSTRAINT [FK_tbParcela_tbConveniado]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'O campo valor ser� utilizado para o c�lculo da comiss�o do conveniado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbParcela', @level2type=N'COLUMN',@level2name=N'valor'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vencimento da parcela' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbParcela', @level2type=N'COLUMN',@level2name=N'vencimento'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campo para relacionar as informa��es da parcela ao Conveniado atrav�s da tabela tbConveniado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbParcela', @level2type=N'COLUMN',@level2name=N'conveniadoId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela para armazenamento com as informa��es da parcela relacionada ao conveniado atrav�s do campo conveniadoId' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbParcela'
GO


/****** Object:  Table [dbo].[tbComissao] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbComissao](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[dataCarga] [datetime] NULL,
	[valorTotalCarga] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tbComissao] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da carga para gera��o da comiss�o' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbComissao', @level2type=N'COLUMN',@level2name=N'dataCarga'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor total da carga gerada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbComissao', @level2type=N'COLUMN',@level2name=N'valorTotalCarga'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela para armazenar informa��es relacionadas a gera��o de carga das comiss�es' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbComissao'
GO

/****** Object:  Table [dbo].[tbComissaoItem] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbComissaoItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[comissaoId] [bigint] NULL,
	[parcelaId] [bigint] NULL,
	[valorPagoComissao] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tbComissaoItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbComissaoItem]  WITH CHECK ADD  CONSTRAINT [FK_tbComissaoItem_tbComissao] FOREIGN KEY([comissaoId])
REFERENCES [dbo].[tbComissao] ([id])
GO

ALTER TABLE [dbo].[tbComissaoItem] CHECK CONSTRAINT [FK_tbComissaoItem_tbComissao]
GO

ALTER TABLE [dbo].[tbComissaoItem]  WITH CHECK ADD  CONSTRAINT [FK_tbComissaoItem_tbParcela] FOREIGN KEY([parcelaId])
REFERENCES [dbo].[tbParcela] ([id])
GO

ALTER TABLE [dbo].[tbComissaoItem] CHECK CONSTRAINT [FK_tbComissaoItem_tbParcela]
GO


/****** Object:  Trigger TGR_COMISSAO ******/

/******
OBSERVA��O
A tabela 'tbParcela' j� deve ter sido criada para a execu��o desta TRIGGER 
******/

CREATE TRIGGER TGR_COMISSAO
ON tbParcela
FOR UPDATE
AS
BEGIN
    DECLARE
	@ID_PARCELA INT
 
	SELECT @ID_PARCELA = id FROM INSERTED

    UPDATE tbComissaoItem SET 
		valorPagoComissao = 
							(select valor from tbParcela where id = @ID_PARCELA) * 
							((select cr.percentual from tbParcela pa, tbConveniado co, tbCorretora cr
							Where pa.conveniadoId = co.corretoraId
							And co.corretoraId = cr.id
							And pa.conveniadoId = (select conveniadoId from tbParcela where id = @ID_PARCELA
							And pa.id = @ID_PARCELA)) / 100)  
	WHERE parcelaId = @ID_PARCELA

	UPDATE tbComissao SET valorTotalCarga = 
							(select valor from tbParcela where id = @ID_PARCELA) 
			WHERE id = (select comissaoId from tbComissaoItem where parcelaId = @ID_PARCELA)
    
END

GO


/****** Object:  StoredProcedure: sp_Gerar_Carga_Comissionado ******/

/******
OBSERVA��O PARA A CRIA��O DA PROCEDURE
As tabelas 'tbComissao' e 'tbComissaoItem' j� devem ter sido criadas para a execu��o desta PROCEDURE 
******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_Gerar_Carga_Comissionado 
	@INdcmValorTotalCarga decimal(18,2),
	@INidParcela int,
	@INidCorretora int
	
As
BEGIN

	SET NOCOUNT ON;
	Insert Into tbComissao
	(
		dataCarga,
		valorTotalCarga
	)
	Values
	(
		GETDATE(),
		@INdcmValorTotalCarga 
	)

	DECLARE @idTbOmissao int;
	SET @idTbOmissao = SCOPE_IDENTITY()

	Insert Into tbComissaoItem
	(
		comissaoId,
		parcelaId,
		valorPagoComissao
	)
	Values
	(
		@idTBomissao,
		@INidParcela,
		@INdcmValorTotalCarga * ((Select cr.percentual From tbCorretora cr, tbParcela pa, tbConveniado cv Where cv.corretoraId = cr.id And pa.conveniadoId = cv.id And  cr.id = @INidCorretora And pa.id = @INidParcela) / 100)
	)

	  return @idTbOmissao
END

GO
